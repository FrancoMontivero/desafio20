"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProductsFaker = exports.removeProduct = exports.updateProduct = exports.addProduct = exports.getProduct = exports.getProducts = void 0;
const socket_1 = require("./socket");
const models_1 = __importDefault(require("../models"));
const faker_1 = __importDefault(require("faker"));
function getProducts() {
    return __awaiter(this, void 0, void 0, function* () {
        const Products = yield models_1.default.Product.find();
        return Products;
    });
}
exports.getProducts = getProducts;
;
function getProduct(id) {
    return __awaiter(this, void 0, void 0, function* () {
        if (Number.isNaN(Number(id)))
            throw new Error("El id del producto debe ser un caracter numerico");
        else if (typeof id !== "number")
            id = parseInt(id);
        const product = models_1.default.Product.find({ id }).exec();
        if (product)
            return product;
        else
            throw new Error("Producto no encontrado");
    });
}
exports.getProduct = getProduct;
function addProduct(title, price, thumbnail) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(title && price && thumbnail))
            throw new Error("Hay parametros vacios o indefinidos");
        if (Number.isNaN(Number(price)))
            throw new Error("El precio no puede contener caracteres no numericos");
        else if (typeof price !== "number")
            price = parseFloat(price);
        const newProduct = { title, price, thumbnail };
        yield models_1.default.Product.insertMany([newProduct]);
        try {
            const Products = yield models_1.default.Product.find();
            socket_1.emitProducts(Products);
        }
        finally {
            return newProduct;
        }
    });
}
exports.addProduct = addProduct;
function updateProduct(id, title, price, thumbnail) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(title && price && thumbnail))
            throw new Error("Hay parametros vacios o indefinidos");
        if (Number.isNaN(Number(id)))
            throw new Error("El id del producto debe ser un caracter numerico");
        else if (typeof id !== "number")
            id = parseInt(id);
        if (Number.isNaN(Number(price)))
            throw new Error("El precio no puede contener caracteres no numericos");
        else if (typeof price !== "number")
            price = parseFloat(price);
        const product = yield models_1.default.Product.find({ id }).exec();
        if (!product)
            throw new Error(`No existe un producto con el id ${id}`);
        const newProduct = { title, price, thumbnail };
        try {
            yield models_1.default.Product.updateSyncProduct({ id }, newProduct);
            const Products = yield models_1.default.Product.find();
            socket_1.emitProducts(Products);
        }
        catch (err) {
            throw err;
        }
        finally {
            return product;
        }
    });
}
exports.updateProduct = updateProduct;
function removeProduct(id) {
    return __awaiter(this, void 0, void 0, function* () {
        if (Number.isNaN(Number(id)))
            throw new Error("El id del producto debe ser un caracter numerico");
        else if (typeof id !== "number")
            id = parseInt(id);
        const product = yield models_1.default.Product.find({ id }).exec();
        if (!product)
            throw new Error(`No existe un producto con el id ${id}`);
        try {
            yield models_1.default.Product.deleteOne({ id });
            const Products = yield models_1.default.Product.find({ id }).exec();
            socket_1.emitProducts(Products);
        }
        finally {
            return product;
        }
    });
}
exports.removeProduct = removeProduct;
function getProductsFaker(amount) {
    if (Number.isNaN(Number(amount)))
        throw new Error("La cantiadad de productos debe ser un caracter numerico");
    else if (typeof amount !== "number")
        amount = parseInt(amount);
    let products = [];
    for (let i = 0; i < amount; i++) {
        const newProduct = {
            "titulo": faker_1.default.commerce.productName(),
            "precio": faker_1.default.commerce.price(),
            "foto": faker_1.default.image.imageUrl()
        };
        products.push(newProduct);
    }
    return products;
}
exports.getProductsFaker = getProductsFaker;
