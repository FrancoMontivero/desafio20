import http from 'http';
import express from 'express';
import { Server, Socket } from "socket.io";

import path from 'path';
import cors from './middlewares/cors';
import multer from './middlewares/multer';
import morgan from './middlewares/morgan';
import errorHandler from './middlewares/errorHandler';

import { initServer } from './controllers/socket';

import { SHOW_REQUEST } from './config';

import router from './routes';

const app = express();
const server = http.createServer(app);
const io = new Server(server);
initServer(io);

app.use(express.json());
app.use(multer.array('any'));
app.use(express.urlencoded({ extended : true }));
app.use('/static', express.static(path.resolve(__dirname, '../assets')));

if(SHOW_REQUEST === 'enabled') app.use(morgan); 

app.use(cors);

app.use(router);
app.use(errorHandler);

export default server;