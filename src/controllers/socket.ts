import { getProducts, ProductInterface } from './products';
import { getMessages, addMessage } from './messages';
import { Server, Socket } from 'socket.io';

let refIo: Server;

export function initServer(io: Server): void {
    refIo = io;
    io.on("connection", async (socket: Socket) => {
        emitProducts(await getProducts());
        emitMessages(await getMessages());

        socket.on("new-message", async (data: {date: string, message: string, from: string}) => {
            const newMessage = await addMessage(new Date(data.date), data.message, data.from);
            if(newMessage) emitNewMessage(newMessage);
        });
    });
}

export function emitProducts(products: Array<ProductInterface>): void {
    refIo.emit("products", products);
};

export async function emitMessages(messages: Object): Promise<void> {
    refIo.emit("messages", messages);
}

export function emitNewMessage(message: {[key: string]: {from: string, message: string, hour: string}}): void {
    refIo.emit("new-message", message);
}